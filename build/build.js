var gui = new dat.GUI();
var params = {
    Taille: 100,
    difference: 1.20,
    angle: 30,
    maxligne: 0,
    hauteur: 500,
    Download_Image: function () { return save(); },
};
gui.add(params, "Download_Image");
gui.add(params, "Taille", 0, 200, 1);
gui.add(params, "difference", 0, 2, 0.01);
gui.add(params, "angle", 0, 360, 1);
gui.add(params, "maxligne", 0, 15, 1);
gui.add(params, "hauteur", 0, 1000, 1);
var ai = new rw.HostedModel({
    url: "https://skygan-47a0428d.hosted-models.runwayml.cloud/v1/",
    token: "j5bo8/DudE+yzJRFFBpVUg==",
});
var ai2 = new rw.HostedModel({
    url: "https://stylegan-ff493f68.hosted-models.runwayml.cloud/v1/",
    token: "Q54IaYFScV8bOXOuJeilWQ==",
});
var ai3 = new rw.HostedModel({
    url: "https://butterflybreeder-c7bfdfbd.hosted-models.runwayml.cloud/v1/",
    token: "p/cvToGaPC38/W+Tvsb87w==",
});
var img;
var img2;
var img3;
var msk;
var gif;
var masked = true;
var opacity = 0;
var x = 0;
var y = 0;
var speed = 1;
function preload() {
    msk = loadImage('Img/Mask1.png');
    gif = loadImage('Img/flap.gif');
}
function draw() {
    if (key == 'a') {
        transition(1);
    }
    if (key == '0') {
        step0();
    }
    if (key == '1') {
        step1();
    }
    if (key == '2') {
        step2();
    }
    if (key == '3') {
        step3();
    }
}
function transition(n) {
    if (opacity < 255) {
        opacity++;
    }
    else {
        opacity = 0;
        key = n;
    }
    fill(10, 10, 10, opacity);
    rect(0, 0, width, height);
}
function step0() {
    background(0);
    fill(255);
    textSize(50);
    stroke('white');
    text("Artificial Dream", 50, 100);
    textSize(30);
    text("By Théo Arnauld des Lions", 500, 800);
    text("And Clément Bourhis", 500, 850);
}
function step1() {
    background(0);
    imageMode(CENTER);
    if (img2)
        image(img2, width / 2, height / 2, mouseX, mouseX);
    imageMode(CORNER);
}
function step2() {
    if (img)
        image(img, 0, 0, 6 * width, height);
    imageMode(CENTER);
    if (img2)
        image(img2, width / 2, height / 2, mouseX, mouseX);
    imageMode(CORNER);
}
function step3() {
    if (img)
        image(img, -3 * mouseX - width / 2, 0, 6 * width, height);
    image(gif, mouseX, width / 3 + 100 * cos(mouseX), 600, 300);
    image(gif, mouseX - 300, width / 3 + 100 * cos(mouseX), 600, 300);
    if (img2)
        image(img2, mouseX, width / 3 + 100 * cos(mouseX), 300, 300);
    angleMode(DEGREES);
    randomSeed(0);
    noStroke();
    translate(width / 2, params.hauteur);
    line(0, 0, 0, -params.Taille);
    ligneV3(-params.Taille, params.maxligne, x, y);
    x += speed;
    y += speed;
}
function make_request(z) {
    var inputs = {
        "z": z,
        "truncation": 0.8,
    };
    ai.query(inputs).then(function (outputs) {
        var image = outputs.image;
        img = createImg(image);
        img.hide();
        z[0] += cos(mouseY);
        z[1] += cos(mouseX);
    });
    ai2.query(inputs).then(function (outputs) {
        var image = outputs.image;
        img2 = createImg(image);
        img2.hide();
    });
    ai3.query(inputs).then(function (outputs) {
        var image = outputs.image;
        img3 = createImg(image);
        img3.hide();
    });
}
function ligneV3(taille, nbligne, x, y) {
    if (nbligne > 0) {
        translate(0, taille);
        push();
        rotate(params.angle + ((height / 2) + x / 8) + y / 8);
        line(0, 0, 0, taille * params.difference * random(0, 10));
        if (img3)
            image(img3, 0, 0, 50 * cos(x * random(0, 10)) * random(0, 1), 50 * sin(y * random(0, 10)) * random(0, 1));
        ligneV3(taille * params.difference, nbligne - 1, x, y);
        pop();
        push();
        rotate(-params.angle + ((height / 2) - x / 8) - y / 8);
        line(0, 0, 0, taille * params.difference);
        ligneV3(taille * params.difference, nbligne - 1, -x, -y);
        pop();
    }
}
function setup() {
    p6_CreateCanvas();
    var z = [];
    for (var i = 0; i < 512; i++) {
        z[i] = random(-0.5, 0.5);
    }
    make_request(z);
}
function windowResized() {
    p6_ResizeCanvas();
}
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map